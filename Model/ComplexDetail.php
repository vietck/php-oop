<?php
require_once 'Detail.php';
require_once 'SingleDetail.php';
require_once './Library/ViFuncs.php';

class ComplexDetail extends Detail
{
    public $size = 0;
    public $lDetails = [];

    public function import($index = "")
    {
        parent::import();
        $this->size = Helper::inputIntRange( 1, 10, '-> Total details of detail ' . $this->id . ': ');

        for ($i = 0; $i < $this->size; $i++) {
            echo "\n^ Detail " . ($index == "" ? "" : ($index . ".")) . ($i + 1) . ' of detail ' .$this->id . ":\n";
            $type = Helper::inputIntRange(1, 2, '-> Type (1-Single|2-Complex): ');

            $md = null;

            if ($type == 1) {
                $md = new SingleDetail();

            } else {
                $md = new ComplexDetail();
            }

            $md->import($i + 1);
            $this->lDetails[] = $md;
        }
    }

    public function export($index = "", $level = 0)
    {
        if ($level == 0) {
            parent::export($index);
            echo    str_pad($this->getPrice(), 30) .
                    str_pad($this->getWeight(), 20) . "\n";
        }

        $i = 1;
        foreach ($this->lDetails as $detail) {
            if ($detail instanceof ComplexDetail) {
                echo    str_pad($index . "." . $i, 20) .
                        str_pad($detail->getID(), 30) .
                        str_pad($detail->getPrice(), 30) .
                        str_pad($detail->getWeight(), 20) . "\n";
                $detail->export($level + 1, $index . "." . $i);
            } else {
                $detail->export($index . "." . $i);
            }
            $i++;
        }
    }

    public function getPrice()
    {
        return Helper::calSumArrDetail($this->lDetails, 'price');
    }

    public function getWeight()
    {
        return Helper::calSumArrDetail($this->lDetails, 'weight');
    }

    public function readFile($file) {
        $line =  str_replace("\n", "", fgets($file));
        if (!empty($line)) {
            $data = explode(' ', $line);
            $this->id = $data[0];
            $this->size = $data[1];

            $i = 0;
            while($i < $this->size) {
                $type = str_replace("\n", "", fgets($file));
                $detail = null;
                if ($type == "c") {
                    $detail = new ComplexDetail();
                } else {
                    $detail = new SingleDetail();
                }
                $detail->readFile($file);
                $this->lDetails[] = $detail;
                $i++;
            }
        }
    }

    public function saveFile($file)
    {
        fwrite($file, "c\n");
        fwrite($file, $this->id . " ");
        fwrite($file, $this->size . "\n");
        foreach ($this->lDetails as $detail) {
            $detail->saveFile($file);
        }
    }

    public function sort($field, $direction)
    {
        $this->lDetails = Helper::quickSort($this->lDetails, $field, $direction);
        foreach ($this->lDetails as $detail) {
            if ($detail instanceof ComplexDetail) {
                $detail->sort($field, $direction);
            }
        }
    }
}

//$md = new ComplexDetail();
//$md->import();
//$md->export();