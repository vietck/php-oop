<?php
require_once 'Machine.php';

class Warehouse
{
    public $name = "";
    public $size = 0;
    public $lMachines = [];

    public function import()
    {
        $this->name = Helper::inputStrNonStr('|', 1, 30, '-> Name warehouse: ');
        $this->size = Helper::inputIntRange(1, 10, '-> Total machines of warehouse "' . $this->name . '": ');

        for ($i = 0; $i < $this->size; $i++) {
            echo "\n~ Machine " . ($i + 1) . ' in warehouse ' . $this->name . ":\n";
            $mc = new Machine();
            $mc->import();
            array_push($this->lMachines, $mc);
        }
    }

    public function export()
    {
        $i = 1;
        $lengthSide = intdiv((140 - strlen($this->name)), 2) - 1;
        $odd = strlen($this->name) % 2;
        $nameWarehouse =    str_repeat('*', $lengthSide) .
                            " " . $this->name . " " .
                            str_repeat('*', $lengthSide + $odd). "\n";
        $totalPrice = "Total price: " . $this->getPrice();
        $totalWeight = "Total weight: " . $this->getWeight();

        $odd = strlen($totalWeight) % 2;
        echo    $nameWarehouse .
                str_pad($totalPrice, strlen($nameWarehouse) - strlen($totalWeight) - 1 - $odd) .
                $totalWeight . "\n" . str_repeat('****', 35) . "\n";

        echo    str_pad('No.', 20) .
                str_pad('ID', 30) .
                str_pad('Price', 30) .
                str_pad('Weight', 20) .
                str_pad('Name', 40) ."\n";

        foreach ($this->lMachines as $machine) {
            $machine->export($i);
            $i++;
        }
    }

    public function getPrice()
    {
        return Helper::calSumArrDetail($this->lMachines, 'price');
    }

    public function getWeight()
    {
        return Helper::calSumArrDetail($this->lMachines, 'weight');
    }

    public function getSize()
    {
        return count($this->lMachines);
    }

    public function reset()
    {
        $this->name = "";
        $this->size = 0;
        $this->lMachines = [];
    }

    public function search()
    {
        echo    "--- Search by ---\n" .
                "1. All\n" .
                "2. Id\n" .
                "3. Name\n";
        $field = Helper::inputIntRange(1, 3, '=> Choose field: ');
        echo "\n";

        $keyWord = Helper::inputStrLimit(1, 30, '-> Key word: ');
        echo "\n";

        echo "* Search result:\n";
        $i = 1;
        $notFound = true;
        foreach ($this->lMachines as $machine) {
            if ($machine->checkMatch($keyWord, $field)) {
                if ($notFound) {
                    echo    str_pad('No.', 20) .
                            str_pad('ID', 30) .
                            str_pad('Price', 30) .
                            str_pad('Weight', 20) .
                            str_pad('Name', 40) . "\n";
                }
                echo ($i != 1 ? "\n" : "");
                $machine->export($i);
                $i++;
                $notFound = false;
            }
        }
        if ($notFound)
            echo "Not found! =(\n";
    }

    public function readFile($path) {
        if (!file_exists($path)) {
            return false;
        }

        $file = fopen($path, "r") or die("Unable to open file!");
        $line = str_replace("\n", "", fgets($file));
        if (!empty($line)) {
            $data = explode('|', $line);
            $this->name = $data[0];
            $this->size = $data[1];

            $i = 0;
            while($i < $this->size) {
                $mc = new Machine();
                $mc->readFile($file);
                $this->lMachines[] = $mc;
                $i++;
            }
        }
        fclose($file);
        return true;
    }

    public function saveFile($path) {
        $file = fopen($path, "w");
        fwrite($file, $this->name . "|");
        fwrite($file, $this->size . "\n");
        foreach ($this->lMachines as $machine) {
            $machine->saveFile($file);
        }
        fclose($file);
        return true;
    }

    public function sort()
    {
        echo    "--- Sort by ---\n" .
                "1. Id\n" .
                "2. Price\n" .
                "3. Weight\n";
        $field = Helper::inputIntRange(1, 3, '=> Choose field: ');
        echo "\n";

        echo    "--- Sort direction ---\n" .
                "1. Ascending\n" .
                "2. Decrease\n";
        $direction = Helper::inputIntRange(1, 2, '=> Choose direction: ');
        echo "\n";

        $this->lMachines = Helper::quickSort($this->lMachines, $field, $direction);

        foreach ($this->lMachines as $machine) {
            $machine->sort($field, $direction);
        }
        echo "* Sort result:\n";
        $this->export();
    }
}