<?php
require_once 'SingleDetail.php';
require_once 'ComplexDetail.php';
require_once './Library/ViFuncs.php';

class Machine
{
    public $id = "";
    public $name = "";
    public $size = 0;
    public $lDetails = [];

    public function getID() {
        return $this->id;
    }

    public function import($index = "")
    {
        $this->id = Helper::inputStrNonPatter('/\s/', 1, 15, '-> Id machine: ');
        $this->name = Helper::inputStrNonStr('|', 1, 30, '-> Name machine: ');
        $this->size = Helper::inputIntRange(1, 10, '-> Total details of machine ' . $this->id . ': ');

        for ($i = 0; $i < $this->size; $i++) {
            echo "\n^ Detail " . ($index == "" ? "" : ($index . ".")) . ($i + 1) . ' of machine ' . $this->id . ":\n";
            $type = Helper::inputIntRange(1, 2, '-> Type (1-Single|2-Complex): ');

            if ($type == 1) {
                $md = new SingleDetail();
            } else {
                $md = new ComplexDetail();
            }

            $md->import($i);
            $this->lDetails[] = $md;
        }
    }

    public function export($index)
    {
        echo    str_repeat('====', 35) . "\n" .
                str_pad($index, 20) .
                str_pad($this->id, 30) .
                str_pad($this->getPrice(), 30) .
                str_pad($this->getWeight(), 20) .
                str_pad($this->name, 40) .
                "\n" . str_repeat('----', 35) . "\n";

        $i = 1;
        foreach ($this->lDetails as $detail) {
            $detail->export($index . '.' . $i);
            $i++;
        }
    }

    public function getPrice()
    {
        return Helper::calSumArrDetail($this->lDetails, 'price');
    }

    public function getWeight()
    {
        return Helper::calSumArrDetail($this->lDetails, 'weight');
    }

    public function checkMatch($keyWord, $index = 0) {
        if ($index == 1) {
            if ((stristr($this->id, $keyWord) !== false) || (stristr($this->name, $keyWord) !== false))
                return true;
        } else if ($index == 2) {
            if (stristr($this->id, $keyWord) !== false)
                return true;
        } else if ($index == 3) {
            if (stristr($this->name, $keyWord) !== false)
                return true;
        }
        return false;
    }

    public function readFile($file) {
        $line =  str_replace("\n", "", fgets($file));
        if (!empty($line)) {
            $data = explode('|', $line);
            $this->id = $data[0];
            $this->name = $data[1];
            $this->size = $data[2];

            $i = 0;
            while($i < $this->size) {
                $type = str_replace("\n", "", fgets($file));
                $detail = null;
                if ($type == "c") {
                    $detail = new ComplexDetail();
                } else {
                    $detail = new SingleDetail();
                }
                $detail->readFile($file);
                $this->lDetails[] = $detail;
                $i++;
            }
        }
    }

    public function saveFile($file)
    {
        fwrite($file, $this->id . "|");
        fwrite($file, $this->name . "|");
        fwrite($file, $this->size . "\n");
        foreach ($this->lDetails as $detail) {
            $detail->saveFile($file);
        }
    }

    public function sort($field, $direction)
    {
        $this->lDetails = Helper::quickSort($this->lDetails, $field, $direction);
        foreach ($this->lDetails as $detail) {
            if ($detail instanceof ComplexDetail) {
                $detail->sort($field, $direction);
            }
        }
    }
}

//$mc = new Machine();
//$mc->import();
//$mc->export();