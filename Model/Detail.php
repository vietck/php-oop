<?php
require_once './Library/ViFuncs.php';

abstract class Detail {
    public $id;

    public function getID() {
        return $this->id;
    }

    public function import() {
        $this->id = Helper::inputStrNonPatter('/\s/', 1, 15, '-> Id: ');
    }

    public function export($index) {
        echo str_pad($index, 20);
        echo str_pad($this->id, 30);
    }

    abstract public function getPrice();
    abstract public function getWeight();
}