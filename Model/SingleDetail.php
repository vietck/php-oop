<?php
require_once 'Detail.php';
require_once './Library/ViFuncs.php';

class SingleDetail extends Detail {
    public $price;
    public $weight;

    public function import() {
        parent::import();
        $this->price = Helper::inputPositiveInt('-> Price: ');
        $this->weight = Helper::inputPositiveInt('-> Weight: ');
    }

    public function export($index) {
        parent::export($index);
        echo    str_pad($this->price, 30) .
                str_pad($this->weight, 20) . "\n";
    }

    public function getPrice() {
        return $this->price;
    }

    public function getWeight() {
        return $this->weight;
    }

    public function readFile($file) {
        $line = str_replace("\n", "", fgets($file));
        if (!empty($line)) {
            $data = explode(' ', $line);
            $this->id = $data[0];
            $this->price = $data[1];
            $this->weight = $data[2];
        }
    }

    public function saveFile($file)
    {
        fwrite($file, "s\n");
        fwrite($file, $this->id . " ");
        fwrite($file, $this->price . " ");
        fwrite($file, $this->weight . "\n");
    }
}

//$md = new SingleDetail(1);
//$md->import();
//$md->export();