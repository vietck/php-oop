<?php

class Helper
{
    public static function calSumArrDetail($lDetails, $property)
    {
        $total = 0;
        foreach ($lDetails as $detail) {
            $total += ($property == 'price' ? $detail->getPrice() : $detail->getWeight());
        }
        return $total;
    }

    public static function getField($list, $index, $field)
    {
        if ($field == 1) {
            return $list[$index]->getID();
        } else if ($field == 2) {
            return $list[$index]->getPrice();
        } else if ($field == 3) {
            return $list[$index]->getWeight();
        }
        return 0;
    }

    public static function quickSort($list, $field, $direction)
    {
        if (count($list) <= 1) {
            return $list;
        } else {
            $pivot = Helper::getField($list, 0, $field);
            $left = [];
            $right = [];
            for ($i = 1; $i < count($list); $i++) {
                if ($direction == 1 ? Helper::getField($list, $i, $field) < $pivot : Helper::getField($list, $i, $field) > $pivot) {
                    $left[] = $list[$i];
                } else {
                    $right[] = $list[$i];
                }
            }
            return array_merge(Helper::quickSort($left, $field, $direction), [$list[0]], Helper::quickSort($right, $field, $direction));
        }
    }

    public static function isInt($value)
    {
        return strval($value) === strval(intval($value));
    }

    public static function inputInt($instruction = "")
    {
        $hasRun = false;
        do {
            if ($hasRun) {
                echo "* The value to enter must be an integer!\n";
            }
            $value = readline($instruction);
            $hasRun = true;
        } while (!self::isInt($value));
        return $value;
    }

    public static function inputPositiveInt($instruction = "")
    {
        $hasRun = false;
        do {
            if ($hasRun) {
                echo "* The value to enter must be a positive integer!\n";
            }
            $value = self::inputInt($instruction);
            $hasRun = true;
        } while ($value < 0);
        return $value;
    }

    public static function inputIntRange($min, $max, $instruction = "")
    {
        $hasRun = false;
        do {
            if ($hasRun) {
                echo "* Enter values must be an integer in the range from $min to $max!\n";
            }
            $value = self::inputInt($instruction);
            $hasRun = true;
        } while ($value < $min || $value > $max);
        return $value;
    }

    public static function inputStrLimit($min, $max, $instruction = "")
    {
        $hasRun = false;
        do {
            if ($hasRun) {
                echo "* The string must be between $min and $max in length!\n";
            }
            $value = trim(readline($instruction), " \0\t\n\x0B\r");
            $hasRun = true;
        } while (strlen($value) < $min || strlen($value) > $max);
        return $value;
    }

    public static function inputStrNonPatter($pattern, $min, $max, $instruction = "")
    {
        $hasRun = false;
        do {
            if ($hasRun) {
                echo "* String cannot contain spaces!\n";
            }
            $value = self::inputStrLimit($min, $max, $instruction);
            $hasRun = true;
        } while (preg_match($pattern, $value));
        return $value;
    }

    public static function inputStrNonStr($str, $min, $max, $instruction = "")
    {
        $hasRun = false;
        do {
            if ($hasRun) {
                echo "* String cannot contain character '|' !\n";
            }
            $value = self::inputStrLimit($min, $max, $instruction);
            $hasRun = true;
        } while (strpos($value, $str) !== false);
        return $value;
    }
}