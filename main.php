<?php
require_once 'Model/Warehouse.php';
require_once 'Library/ViFuncs.php';

class Main
{
    private $option = -1;
    private $run = true;
    private $isOpen = false;
    private $isEditing = false;
    private $pathData = "Data/";
    private $pathFile = "";
    private $warehouse;

    public function __construct() {
        $this->warehouse = new Warehouse();
    }

    private function newFile()
    {
        $this->warehouse->reset();
        $this->pathFile = "";
        $this->isOpen = true;
    }

    private function openFile()
    {
        $this->pathFile = Helper::inputStrLimit(1, 100, '-> Enter path to open: ');
        if ($this->warehouse->readFile($this->pathData . $this->pathFile)) {
            $this->isOpen = true;
            echo "* Read successfully!\n\n";
        } else {
            echo "* The file does not exist!\n\n";
        }
    }

    private function saveFile()
    {
        $this->pathFile = Helper::inputStrLimit(1, 100, '-> Enter path to save: ');
        if ($this->warehouse->saveFile($this->pathData . $this->pathFile)) {
            echo "* Saved successfully!\n";
        } else {
            echo "* Save failed!\n";
        }
        $this->isEditing = false;
    }

    private function closeFile()
    {
        $option = null;
        if ($this->isEditing) {
            echo "\nDo you want to save the current data?\n";
            echo "1. Yes\n";
            echo "2. No\n";
            echo "0. Cancel\n";
            $option = Helper::inputIntRange(0, 2, '=> Choose function: ');
        }

        if ($option == 1) {
            $this->saveFile();
        }

        if (!$this->isEditing || $option == 2) {
            $this->pathFile = "";
            $this->warehouse->reset();
            $this->isOpen = false;
            $this->isEditing = false;
        }
    }

    public function runMainProgram()
    {
        echo "********** MACHINES MANAGEMENT VER_1.0 **********\n\n";

        while ($this->run) {
            if (!$this->isOpen) {
                echo "===== MENU FILE =====\n";
                echo "1. New Warehouse\n";
                echo "2. Open Warehouse\n";
                echo "0. Exit\n";

                $this->option = Helper::inputIntRange(0, 2, '=> Choose function: ');
                echo "\n";

                switch ($this->option) {
                    case 1:
                        $this->newFile();
                        break;

                    case 2:
                        $this->openFile();
                        break;

                    case 0:
                        $this->run = false;
                        break;

                    default:
                        echo "Function does not exist!\n";
                }
            } else {
                echo "===== MENU FUNCTIONS =====\n";
                echo "1. Add list machines\n";
                echo "2. Show list machines\n";
                echo "3. Sort machines\n";
                echo "4. Search machines\n";
                echo "5. Save list\n";
                echo "0. Close file\n";

                $this->option = Helper::inputIntRange(0, 5, '=> Choose function: ');
                echo "\n";

                switch ($this->option) {
                    case 1:
                        $this->warehouse->import();
                        $this->isEditing = true;
                        break;

                    case 2:
                        $this->warehouse->export();
                        break;

                    case 3:
                        $this->warehouse->sort();
                        $this->isEditing = true;
                        break;

                    case 4:
                        $this->warehouse->search();
                        break;

                    case 5:
                        $this->saveFile();
                        break;

                    case 0:
                        $this->closeFile();
                        break;

                    default:
                        echo "Function does not exist!\n";
                }
                echo "\n";
            }
        }
    }
}

$main = new Main();
$main->runMainProgram();